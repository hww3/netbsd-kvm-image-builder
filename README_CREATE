1. Create Guest Tools Image
	./create-netbsd-guesttools 7.1

2. Create Empty VM and boot NetBSD install

Download NetBSD install ISO (7.1 is demonstrated here, 6.1.5 also works.)

Empty vm config /var/tmp/vm.json: 

{
  "brand": "kvm",
  "vcpus": 1,
  "autoboot": false,
  "ram": 4096,
  "disks": [
    {
      "boot": true,
      "model": "virtio",
      "size": 20480
    }
  ],
  "nics": [
    {
      "nic_tag": "external",
      "ip": "dhcp",
      "primary": "true",
      "model": "virtio"
    }
  ]
}

vmadm create /var/tmp/vm.json
UUID=/uuid of newly created vm/

cp guest tools iso and the NetBSD installer iso to the root of your kvm zone (/zones/$UUID/root).

vmadm start $UUID order=cd,once=d cdrom=/NetBSD-7.1-amd64.iso,ide cdrom=/netbsd-7.1-guesttools.iso,ide
vmadm info $UUID vnc

Connect your VNC viewer to the specified host/port/display

Take the defaults during the install process, with the following exceptions:

Install minimal with man pages
Enable sshd, ntpd
Disable raidframe
Don't configure networking

After the install process is complete, exit the install program, which should drop you to a command prompt.

# mount the newly installed netbsd root disk
mkdir /tmp/root
# mount the netbsd guest tools image
mkdir /tmp/cdrom
mount /dev/ld0a /tmp/root
mount -t cd9660 /dev/cd1a /tmp/cdrom
cp /tmp/cdrom/* /tmp/root
#install triton guest tools
chroot /tmp/root
tar xzvf triton*gz
rm triton*gz
pkg_add *tgz
rm *tgz
# enable serial console for use with "vmadm console UUID"
cp /boot.cfg boot.cfg.bak
sed -e 's/^timeout=5/timeout=10/' < boot.cfg.bak > boot.cfg
echo consdev=com0 >> /boot.cfg
rm /boot.cfg.bak
# allow root login
echo PermitRootLogin yes >> /etc/ssh/sshd_config


Once this is done, don't reboot. Switch back to the headnode and create the image and manifest:

VER=6.1.5
SIZE=10240
DATE=`date +%Y%m%d`
# BUILDZONE is the UUID of the zone that contains the image builder project.
BUILDZONE=298a43d0-6600-6ccd-af5c-919fdfe58fe7
vmadm stop $UUID
cd /var/tmp
zfs snap zones/$UUID-disk0@snap
zfs send zones/$UUID-disk0@snap > netbsd-$VER.zvol
gzip netbsd-$VER.zvol
/zones/$BUILDZONE/root/data/netbsd-kvm-image-builder/create-image-manifest -f netbsd-$VER.zvol.gz -n netbsd-$VER \
  -s $SIZE -v $DATE -o bsd > netbsd-$VER.manifest
# check the manifest and zvol file to make sure they seem reasonable.

# import the newly created image into your SDC install
sdc-imgadm import -m netbsd-$VER.manifest -f netbsd-$VER.zvol.gz

If all goes well, you can then delete the build vm:

vmadm destroy $UUID


