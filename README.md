# netbsd-kvm-image-builder

This repo creates a custom NetBSD install ISO and a KVM image for use in SmartOS and Triton.

## Requirements

You'll need a non-root zone with cdrtools installed in order to create the guest tools image.
The rest of the work will be done from the global zone of the head node.

## Setup

The following packages are required:

```
pkgin in bash cdrtools
```

## Usage
 
This is a work in progress so much of the process is manual. See the README_CREATE file for a step
by step description of how to prepare the image.
